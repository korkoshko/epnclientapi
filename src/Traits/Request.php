<?php

namespace korkoshko\EpnClientApi\Traits;

use korkoshko\EpnClientApi\Exceptions\EpnClientException;

/**
 * Trait Request
 *
 * @package korkoshko\EpnClientApi\Traits
 */
trait Request
{
    /** Send post request
     *
     * @param string $url
     * @param array $params
     *
     * @return array
     * @throws EpnClientException
     */
    protected function request(string $url, array $params): array
    {
        $ch     = curl_init($url);

        $params = json_encode($params);

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $params,
        ]);

        $response = $this->getResponse($ch);

        curl_close($ch);

        return $response;
    }

    /**
     * Get response to the request
     *
     * @param $ch
     *
     * @return array
     * @throws EpnClientException
     */
    protected function getResponse($ch): array
    {
        $response = curl_exec($ch);

        if (!$response) {
            throw new EpnClientException('Request failed: Error ' . curl_errno($ch));
        }

        $response = json_decode($response, true);

        if (!empty($response['error'])) {
            throw new EpnClientException('API Error: ' . $response['error']);
        }

        return $response['results'];
    }
}