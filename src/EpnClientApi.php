<?php

namespace korkoshko\EpnClientApi;

use korkoshko\EpnClientApi\Traits\{
    Methods,
    Request
};

use korkoshko\EpnClientApi\Exceptions\EpnClientException;

class EpnClientApi
{
    /**
     *  Epn API url
     *
     * @@var string
     */
    const API_URL     = 'http://api.epn.bz/json';

    /**
     *  Epn API version
     *
     * @@var int
     */
    const API_VERSION = 2;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string
     */
    private $userHash;

    /**
     * @var array
     */
    private $requests = [];

    use Methods, Request;

    /**
     * EpnClientApi constructor.
     * @param string|null $accessToken
     * @param string|null $userHash
     */
    public function __construct(string $accessToken = null, string $userHash = null)
    {
        $this->accessToken   = $accessToken;
        $this->userHash      = $userHash;
    }

    /**
     * Set access token / api key
     *
     * @param string $accessToken
     *
     * @return EpnClientApi
     * @throws EpnClientException
     */
    public function setAccessToken(string $accessToken)
    {
        if (empty($apiKey)) {
            throw new EpnClientException("Param 'apiKey' is empty!");
        }

        $this->accessToken = $apiKey;

        return $this;
    }

    /**
     * Set user hash
     *
     * @param string $userHash
     *
     * @return EpnClientApi
     * @throws EpnClientException
     */
    public function setUserHash(string $userHash)
    {
        if (empty($userHash)) {
            throw new EpnClientException("Param 'userHash' is empty!");
        }

        $this->userHash = $userHash;

        return $this;
    }

    /**
     * Set special params to request
     *
     * @param array $params
     *
     * @return EpnClientApi
     */
    public function params(array $params)
    {
        $subRequest = end($this->requests);
        $subRequest->addParams($params);

        return $this;
    }

    /**
     * Execute request to API
     *
     * @return array|null
     * @throws EpnClientException
     */
    public function get()
    {
        $requests = $this->getRequests();

        if (empty($requests)) {
            return null;
        }

        if (empty($this->accessToken) || empty($this->userHash)) {
            throw new EpnClientException("Property 'accessToken' or 'userHash' is empty!");
        }

        $this->requests = [];

        return $this->request(self::API_URL, [
            'user_api_key' => $this->accessToken,
            'user_hash'    => $this->userHash,

            'requests'     => $requests,

            'api_version'  => self::API_VERSION,
        ]);
    }

    /**
     *  Add request when one of the api methods was called
     *
     * @param $name
     * @param $args
     */
    protected function callMethod($name, $args)
    {
        $requestId             = $args[0] ?? name;

        $this->requests[]      = new RequestMethod($requestId, [
            'action' => $this->methods[$name]
        ]);
    }

    /**
     *  Get array of requests with a unique name for batch sending
     *
     * @return array
     */
    protected function getRequests()
    {
        $requests = [];

        foreach ($this->requests as $request) {
            $requests[$request->getName()] = $request->getParams();
        }

        return $requests;
    }
};